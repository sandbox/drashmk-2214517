
Gravatar favicon module lets you use your Gravatar image
 as shortcut icon, or 'favicon'.

Gravatar favicon is written by Dragan Atanasov <info@datanasov.com>

Install
-------
Simply install Gravatar favicon like you would any other module.

1) Copy the gravatar_favicon folder to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules).

3) No additional configuration is necessary though you may fine-tune settings
 at Administer -> Appearance -> Settings  -> Current theme
 (/admin/appearance/settings).
   
A note about the browser compatibility
--------------------
Because Internet Explorer 10 and earlier does not support PNG favicon,
 it uses default favicon.
